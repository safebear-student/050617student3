import base_test


class TestCases(base_test.BaseTest):

    def test_01_test_login_page_login(self):
        # Step 1: Click on login and Login page loads
        assert self.welcomepage.click_login(self.loginpage)
        #Step 2: Login to webpage and confirm the main page appears
        assert self.loginpage.login(self.mainpage, "testuser", "testing")
        #Step 3: Logout
        assert self.mainpage.logout(self.welcomepage)

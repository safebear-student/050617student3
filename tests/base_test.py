import unittest

import time

from utils import Parameters
from page10_welcome_page import WelcomePage
from page20_login_page import LoginPage
from page30_main_page import MainPage
from page40_frames_page import FramesPage


class BaseTest(unittest.TestCase):
    param = Parameters()
    welcomepage = WelcomePage(param.w, param.rootURL)
    loginpage = LoginPage(param.w, param.rootURL)
    mainpage = MainPage(param.w, param.rootURL)
    framespage = FramesPage(param.w, param.rootURL)

    def setUp(self):
        self.param.w.get(self.param.rootURL)
        self.param.w.maximize_window()
        assert self.welcomepage.check_page()

    def tearDown(self):
        time.sleep(4)
        self.param.w.quit()


@classmethod
def tearDownClass(cls):
    cls.param.w.quit()
